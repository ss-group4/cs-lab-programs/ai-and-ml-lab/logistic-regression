import numpy as np
import pandas as pd
from sklearn.datasets import load_iris
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix

# Load the Iris dataset
iris = load_iris()

# Get the setosa data
X_setosa = iris.data[iris.target == 0]
y_setosa = iris.target[iris.target == 0]

# Split the data into training and testing sets
X_train_setosa, X_test_setosa, y_train_setosa, y_test_setosa = train_test_split(X_setosa, y_setosa, test_size=0.25)

# Standardize the features


# Initialize the weights and bias
w = np.zeros(X_train_setosa.shape[1])
b = 0


# Calculate the sigmoid function
def sigmoid(z):
    return 1 / (1 + np.exp(-z))


# Calculate the loss function
def loss(y_pred, y_true):
    return -np.mean(y_true * np.log(y_pred) + (1 - y_true) * np.log(1 - y_pred))


# Update the weights and bias using gradient descent
def update_parameters(w, b, X, y, learning_rate):
    z = np.dot(X, w) + b
    y_pred = sigmoid(z)
    gradient_w = np.dot(X.T, (y_pred - y)) / len(y)
    gradient_b = np.mean(y_pred - y)
    w = w - learning_rate * gradient_w
    b = b - learning_rate * gradient_b
    return w, b


# Train the model
epochs = 1000
learning_rate = 0.01
for epoch in range(epochs):
    w, b = update_parameters(w, b, X_train_setosa, y_train_setosa, learning_rate)

# Classify the test data
y_pred_setosa = sigmoid(np.dot(X_test_setosa, w) + b)
y_pred_setosa = np.where(y_pred_setosa > 0.5, 1, 0)

# Confusion matrix
cm = confusion_matrix(y_test_setosa, y_pred_setosa)
print(cm)

# Evaluate the model
accuracy = np.mean(y_pred_setosa == y_test_setosa)
print('Accuracy:', accuracy)
